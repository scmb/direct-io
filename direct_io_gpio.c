//#define pr_fmt(fmt) KBUILD_MODNAME ":" __FILE__ ":" __LINE__ ": " fmt
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/input.h>

#include <linux/kthread.h>  // for threads
#include <linux/delay.h>

#include <linux/leds.h> // for led driver 
#include <linux/platform_device.h> 

#define DIRECT_IO_GPIO_ADDRESS 0x284

struct resource * ioregion;
static char *devname = "direct-io-input-gpio";
static char *thrdname = "direct-io-input-thrd";
static struct input_dev * direct_io_input_dev;	
static struct task_struct * gpio_thrd;

static unsigned short gpio_value = 0x00;

static int INPUT_ON_KEY_VALUE = 0;
static int INPUT_OFF_KEY_VALUE = 1;

static unsigned short gpio_address = DIRECT_IO_GPIO_ADDRESS;

/* 
 * module_param(foo, int, 0000)
 * The first param is the parameters name
 * The second param is it's data type
 * The final argument is the permissions bits, 
 * for exposing parameters in sysfs (if non-zero) at a later stage.
 */

module_param( gpio_address, short, 0444 );
MODULE_PARM_DESC( gpio_address, "Base address of Direct I/O GPIO [0x0000 - 0xFFFF]" );

module_param( gpio_value, short, 0444 );
MODULE_PARM_DESC( gpio_value, "Starting value for GPIO ( affects only outputs )" );


static unsigned short get_bitmask(int led_id)
{
	unsigned short led_mask = 0x00;
	switch ( led_id )
	{	
		case 1 : 
			led_mask = 0x10;
			break;
		case 2 :
			led_mask = 0x20;
			break;
		case 3 :
			led_mask = 0x40;
			break;
		case 4 :
			led_mask = 0x80;
			break;
		default:
			led_mask = 0x00;
	}
	return led_mask;	
}

static void set_led_brightness(int led_id, short value)
{	
	unsigned short mask = gpio_value;
	unsigned short led_mask = get_bitmask(led_id);
	if ( value )
		mask = mask | led_mask ; // ON
	else
		mask = mask & ~led_mask ; // OFF 
	
	pr_info( "set led  0x%02x, mask 0x%02x, gpio 0x%02x, val 0x%02x \n", led_id, led_mask, gpio_value, mask );

	outb(mask, gpio_address);
}

static short get_led_brightness ( int led_id ) 
{
	short bri = LED_OFF;
	unsigned short led_mask = get_bitmask(led_id);
	bri = ( (led_mask & gpio_value) == led_mask  ) ? LED_FULL : LED_OFF;
	pr_info( "get led  0x%02x, mask 0x%02x, gpio 0x%02x, val 0x%02x \n", led_id, led_mask, gpio_value, bri );
	return bri;
}

static int get_led_id_from_name(const char * led_name)
{
	char c ;
	c = led_name[(strlen(led_name)-1)];
	return  c - 0x30 ; 
}

static void direct_io_led_set_brightness(struct led_classdev *cdev, enum led_brightness brightness)
{
	short value;
        pr_debug("Set brightness on %s at %d\n", cdev->name, (brightness == LED_OFF)?LED_OFF:LED_FULL );
	value = (brightness == LED_OFF)?0:1;
	set_led_brightness( get_led_id_from_name(cdev->name), value);
}

static enum led_brightness direct_io_led_get_brightness(struct led_classdev *cdev)
{
        pr_debug("Get brightness on %s\n", cdev->name );
	return get_led_brightness( get_led_id_from_name( cdev->name ) );
}


static struct led_classdev direct_io_led_1 =
        {
                .name           = "direct_io_gpio:1",
                .brightness_set = direct_io_led_set_brightness,
                .brightness_get = direct_io_led_get_brightness,
        };

static struct led_classdev direct_io_led_2 =
        {
                .name           = "direct_io_gpio:2",
                .brightness_set = direct_io_led_set_brightness,
                .brightness_get = direct_io_led_get_brightness,
        };

static struct led_classdev direct_io_led_3 =
        {
                .name           = "direct_io_gpio:3",
                .brightness_set = direct_io_led_set_brightness,
                .brightness_get = direct_io_led_get_brightness,
        };

static struct led_classdev direct_io_led_4 =
        {
                .name           = "direct_io_gpio:4",
                .brightness_set = direct_io_led_set_brightness,
                .brightness_get = direct_io_led_get_brightness,
        };

static int direct_io_led_probe(struct platform_device *pdev)
{
	int ret;

        ret = led_classdev_register(&pdev->dev, &direct_io_led_1);
	if (ret < 0)
		goto fail_led_1;
        
	ret = led_classdev_register(&pdev->dev, &direct_io_led_2);
	if (ret < 0)
		goto fail_led_2;
        
	ret = led_classdev_register(&pdev->dev, &direct_io_led_3);
	if (ret < 0)
		goto fail_led_3;
        
	ret = led_classdev_register(&pdev->dev, &direct_io_led_4);
	if (ret < 0)
		goto fail_led_4;

	return ret;

	fail_led_4:
		led_classdev_unregister(&direct_io_led_3);
	fail_led_3:
		led_classdev_unregister(&direct_io_led_2);
	fail_led_2:
		led_classdev_unregister(&direct_io_led_1);
	fail_led_1:

	return ret;
	
}

static int direct_io_led_remove(struct platform_device *pdev)
{
	led_classdev_unregister(&direct_io_led_1);
	led_classdev_unregister(&direct_io_led_2);
	led_classdev_unregister(&direct_io_led_3);
	led_classdev_unregister(&direct_io_led_4);
        return 0;
}

static struct platform_driver direct_io_led_driver = {
        .probe = direct_io_led_probe,
        .remove = direct_io_led_remove,
        .driver = {
                .name = "direct-io-led",
                .owner = THIS_MODULE,
        },
};

static struct platform_device direct_io_led_device = {
       .name   = "direct-io-led",
       .id     = 0,
};

static int led_init(void) {
	int ret;
	
	if ((ret = platform_driver_register(&direct_io_led_driver)) != 0) {
		return ret;
	}
	
	if ((ret = platform_device_register(&direct_io_led_device)) != 0) {
		platform_driver_unregister(&direct_io_led_driver);
	}
	
	return ret;
}

static void led_remove(void) {
	platform_device_unregister(&direct_io_led_device);
	platform_driver_unregister(&direct_io_led_driver);
}

static void mask_flag_checker(unsigned mask, unsigned flag, unsigned key )
{
	if( (mask & flag) == flag )
	{
		input_report_key(direct_io_input_dev, key, INPUT_ON_KEY_VALUE);	
	}
	else
	{
		input_report_key(direct_io_input_dev, key, INPUT_OFF_KEY_VALUE);
	}
}

static void input_to_events(void)
{
	unsigned mask = 0x00;
	mask = inb(gpio_address);
	mask_flag_checker(mask, 0x01, KEY_F1);
	mask_flag_checker(mask, 0x02, KEY_F2);
	mask_flag_checker(mask, 0x04, KEY_F8);
	mask_flag_checker(mask, 0x08, KEY_F9);
	input_sync(direct_io_input_dev);
	gpio_value = mask;
}

int gpio_checker_function(void * obj) {
	pr_info( "Input thread [%s] started\n", thrdname);
	
	while(!kthread_should_stop())
	{
		input_to_events();
                msleep_interruptible(10); 
	}	
	pr_info( "Exit thread [%s]\n", thrdname);

	return 0;
}

int gpio_checker_init (void) {
    pr_info( "Initialize GPIO checker thread\n");
    gpio_thrd = kthread_create(gpio_checker_function,NULL,thrdname);
    if((gpio_thrd))
    {
        pr_info( "gpio_thrd: True \n");
        wake_up_process(gpio_thrd);
    }

    return 0;
}



void gpio_checker_cleanup(void) {
   // clean up thread
   kthread_stop( gpio_thrd );
}

static int __init direct_io_gpio_init(void)
{
	int a = 0;
	pr_info( "Direct I/O gpio address: 0x%04x\n", gpio_address );
	a = check_region(gpio_address, 1);
	ioregion = request_region(gpio_address, 1, devname );

	pr_info( "initialize driver\n");
	
	// Initialize outputs
	outb(gpio_value,gpio_address);
	// Read input
	gpio_value = inb(gpio_address);
	
	pr_info( "GPIO value: [0x%02x]\n", gpio_value);

	direct_io_input_dev = input_allocate_device();
	if (!direct_io_input_dev) {
		pr_info("Not enough memory\n");
//		error = -ENOMEM;
//		goto err_free_irq;
	}

	direct_io_input_dev->name = devname;

	/* register a input_dev for KEY_F9 
	 *                          KEY_F10
	 *                          KEY_F11
	 *                          KEY_F12
	 */

	/* set the event type */
	direct_io_input_dev->evbit[0] = BIT(EV_KEY);

	/* set the event codes */
	set_bit(KEY_F1, direct_io_input_dev->keybit);
	set_bit(KEY_F2, direct_io_input_dev->keybit);
	set_bit(KEY_F8, direct_io_input_dev->keybit);	
	set_bit(KEY_F9, direct_io_input_dev->keybit);	

	input_register_device(direct_io_input_dev);

	input_to_events();
	
	gpio_checker_init();
	
	led_init();

	return 0;
}

static void __exit direct_io_gpio_end(void)
{
	pr_info( "end driver\n");
	led_remove();
	gpio_checker_cleanup();
	release_region(gpio_address,1);
	input_unregister_device(direct_io_input_dev);
}

module_init(direct_io_gpio_init);
module_exit(direct_io_gpio_end);

MODULE_AUTHOR("Simone Camporeale <simone.camporeale@gmail.com>");
MODULE_DESCRIPTION("direct_io_gpio");
MODULE_LICENSE("GPL");
